/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */

//
// Handoff scenario with Multipath TCPed iperf
//
// Simulation Topology:
// Scenario: H1 has 3G
//           during movement, MN keeps iperf session to SV.
//
//   <--------------------            ----------------------->
//                  LTE               Ethernet
//                   sim0 +----------+ sim1
//                  +------|  LTE  R  |------+
//                  |     +----------+      |
//              +---+                       +-----+
//          sim0|                                 |sim0
//     +----+---+                                 +----+---+
//     |   H1   |                                 |   H2   |
//     +---+----+                                 +----+---+
//          sim1|                                 |sim1
//              +--+                        +-----+
//                 | sim0 +----------+ sim1 |
//                  +-----|  WiFi R  |------+
//                        +----------+      
//                  WiFi              Ethernet
//   <--------------------            ----------------------->

#include "ns3/network-module.h"
#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/dce-module.h"
#include "ns3/tap-bridge-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/wifi-module.h"
#include "ns3/lte-module.h"
#include "ns3/mobility-module.h"
#include "ns3/applications-module.h"
#include "ns3/netanim-module.h"
#include "ns3/constant-position-mobility-model.h"
#include "ns3/config-store-module.h"
#include "ns3/csma-module.h"

using namespace ns3;
NS_LOG_COMPONENT_DEFINE ("DceMptcpLteWifi");


void setDirection (Ptr<Node> n, int velx, int vely, int velz, int posx, int posy, int posz)
{
    Ptr<ConstantVelocityMobilityModel> dir = CreateObject<ConstantVelocityMobilityModel> ();
    n->AggregateObject (dir);
    Vector dirVec2 (velx, vely, velz);
    Vector locVec2 (posx, posy, posz);
    dir->SetPosition (locVec2);
    dir->SetVelocity (dirVec2);
}

void setPos (Ptr<Node> n, int x, int y, int z)
{
    Ptr<ConstantPositionMobilityModel> loc = CreateObject<ConstantPositionMobilityModel> ();
    n->AggregateObject (loc);
    Vector locVec2 (x, y, z);
    loc->SetPosition (locVec2);
}

void
PrintTcpFlags (std::string key, std::string value)
{
    NS_LOG_INFO (key << "=" << value);
}

int main (int argc, char *argv[])
{
    LogComponentEnable ("DceMptcpLteWifi", LOG_LEVEL_ALL);
    std::string bufSize = "";
    bool disWifi = false;
    bool disLte = false;
    double stopTime = 45.0;
    std::string p2pdelay = "10ms";

    CommandLine cmd;
    cmd.AddValue ("bufsize", "Snd/Rcv buffer size.", bufSize);
    cmd.AddValue ("disWifi", "Disable WiFi.", disWifi);
    cmd.AddValue ("disLte", "Disable LTE.", disLte);
    cmd.AddValue ("stopTime", "StopTime of simulatino.", stopTime);
    cmd.AddValue ("p2pDelay", "Delay of p2p links. default is 10ms.", p2pdelay);
    cmd.Parse (argc, argv);

    if (disWifi && disLte)
    {
        NS_LOG_INFO ("no active interface");
        return 0;
    }

    GlobalValue::Bind ("SimulatorImplementationType", StringValue ("ns3::RealtimeSimulatorImpl"));
    GlobalValue::Bind ("ChecksumEnabled", BooleanValue (true));
    NodeContainer nodes, routers, tapNodes;
    nodes.Create (6);
    routers.Create (1);
    tapNodes.Create (3);

    DceManagerHelper dceManager;
    dceManager.SetNetworkStack ("ns3::LinuxSocketFdFactory",
                                "Library", StringValue ("liblinux.so"));
    LinuxStackHelper stack;
    stack.Install (nodes);
    stack.Install (tapNodes.Get(0));
    stack.Install (routers);

    dceManager.Install (nodes);
    dceManager.Install (tapNodes.Get(0));
    dceManager.Install (routers);

    PointToPointHelper pointToPoint;
    NetDeviceContainer devices1, devices2, devices3, devices4, devices5;
    Ptr<LteHelper> lteHelper = CreateObject<LteHelper> ();
    Ptr<PointToPointEpcHelper> epcHelper = CreateObject<PointToPointEpcHelper> ();
    YansWifiPhyHelper phy;

    Ipv4AddressHelper address1, address2, address3, address4, address5;
    std::ostringstream cmd_oss;
    address1.SetBase ("10.1.0.0", "255.255.255.0");
    address2.SetBase ("10.2.0.0", "255.255.255.0");
    address3.SetBase ("10.3.0.0", "255.255.255.0");
    address4.SetBase ("10.4.0.0", "255.255.255.0");
    address5.SetBase ("10.5.0.0", "255.255.255.0");
    Ipv4InterfaceContainer if1, if2, if3, if4, if5;
    pointToPoint.SetDeviceAttribute ("DataRate", StringValue ("2Mbps"));
    pointToPoint.SetChannelAttribute ("Delay", StringValue (p2pdelay));
    Ptr<RateErrorModel> em1 =
            CreateObjectWithAttributes<RateErrorModel> ("RanVar", StringValue ("ns3::UniformRandomVariable[Min=0.0|Max=1.0]"),
                                                        "ErrorRate", DoubleValue (0.01),
                                                        "ErrorUnit", EnumValue (RateErrorModel::ERROR_UNIT_PACKET)
            );

    setPos (nodes.Get (0), -20, 30 / 2, 0);
    setPos (nodes.Get (1), 100, 30 / 2, 0);
    setPos (nodes.Get (2), 200, 30 / 2, 0);

    // Left link: H1 <-> LTE-R
    NodeContainer enbNodes;
    enbNodes.Create(1);
    dceManager.Install (enbNodes);

    lteHelper->SetEpcHelper (epcHelper);
    Ptr<Node> pgw = epcHelper->GetPgwNode ();
    dceManager.Install(pgw);
    setPos (enbNodes.Get (0), 60, -4000, 0);

    NetDeviceContainer enbLteDevs = lteHelper->InstallEnbDevice (enbNodes);
    NetDeviceContainer ueLteDevs = lteHelper->InstallUeDevice (nodes.Get (0));

    // Assign ip addresses
    if1 = epcHelper->AssignUeIpv4Address (NetDeviceContainer (ueLteDevs));
    lteHelper->Attach (ueLteDevs.Get(0), enbLteDevs.Get(0));


    // setup ip routes
    cmd_oss.str ("");
    cmd_oss << "rule add from " << if1.GetAddress (0, 0) << " table " << 1;
    LinuxStackHelper::RunIp (nodes.Get (0), Seconds (0.1), cmd_oss.str ().c_str ());
    cmd_oss.str ("");
    cmd_oss << "route add default via " << "7.0.0.1 "  << " dev sim" << 0 << " table " << 1;
    LinuxStackHelper::RunIp (nodes.Get (0), Seconds (0.1), cmd_oss.str ().c_str ());

    // LTE-R <-> H2
    // Right link
    devices2 = pointToPoint.Install (nodes.Get (1), pgw);
    devices2.Get (0)->SetAttribute ("ReceiveErrorModel", PointerValue (em1));
    // Assign ip addresses
    if2 = address2.Assign (devices2);
    address2.NewNetwork ();
    // setup ip routes
    cmd_oss.str ("");
    cmd_oss << "rule add from " << if2.GetAddress (0, 0) << " table " << (1);
    LinuxStackHelper::RunIp (nodes.Get (1), Seconds (0.1), cmd_oss.str ().c_str ());
    cmd_oss.str ("");
    cmd_oss << "route add 10.2." << 0 << ".0/24 dev sim" << 0 << " scope link table " << (1);
    LinuxStackHelper::RunIp (nodes.Get (1), Seconds (0.1), cmd_oss.str ().c_str ());
    setPos (pgw, 70, 0, 0);


    // Left link: H1 <-> WiFi-R
    WifiHelper wifi = WifiHelper::Default ();
    phy = YansWifiPhyHelper::Default ();
    YansWifiChannelHelper phyChannel = YansWifiChannelHelper::Default ();
    NqosWifiMacHelper mac;
    phy.SetChannel (phyChannel.Create ());
    mac.SetType ("ns3::AdhocWifiMac");
    wifi.SetStandard (WIFI_PHY_STANDARD_80211a);
    devices1 = wifi.Install (phy, mac, NodeContainer (nodes.Get (0), routers.Get (0)));
        // Assign ip addresses
    if1 = address1.Assign (devices1);
    address1.NewNetwork ();
    // setup ip routes
    cmd_oss.str ("");
    cmd_oss << "rule add from " << if1.GetAddress (0, 0) << " table " << 2;
    LinuxStackHelper::RunIp (nodes.Get (0), Seconds (0.1), cmd_oss.str ().c_str ());
    cmd_oss.str ("");
    cmd_oss << "route add 10.1." << 0 << ".0/24 dev sim"
            << devices1.Get (0)->GetIfIndex () << " scope link table " << 2;
    LinuxStackHelper::RunIp (nodes.Get (0), Seconds (0.1), cmd_oss.str ().c_str ());
    cmd_oss.str ("");
    cmd_oss << "route add default via " << if1.GetAddress (1, 0) << " dev sim"
            << devices1.Get (0)->GetIfIndex () << " table " << 2;
    LinuxStackHelper::RunIp (nodes.Get (0), Seconds (0.1), cmd_oss.str ().c_str ());
    cmd_oss.str ("");
    cmd_oss << "route add 10.1.0.0/16 via " << if1.GetAddress (0, 0) << " dev sim0";
    LinuxStackHelper::RunIp (routers.Get (0), Seconds (0.2), cmd_oss.str ().c_str ());

    // Global default route
    if (disLte)
    {
        cmd_oss.str ("");
        cmd_oss << "route add default via " << if1.GetAddress (1, 0) << " dev sim"
                << devices1.Get (0)->GetIfIndex ();
        LinuxStackHelper::RunIp (nodes.Get (0), Seconds (0.1), cmd_oss.str ().c_str ());
    }

    // Down/Up
#if 0
    cmd_oss.str ("");
  cmd_oss << "link set down dev sim1";
  LinuxStackHelper::RunIp (nodes.Get (0), Seconds (1.0), cmd_oss.str ().c_str ());
  cmd_oss.str ("");
  cmd_oss << "link set up dev sim1";
  LinuxStackHelper::RunIp (nodes.Get (0), Seconds (10.0), cmd_oss.str ().c_str ());
#endif

    // WiFi-R <-> H2
    // Right link
    devices2 = pointToPoint.Install (nodes.Get (1), routers.Get (0));
    devices2.Get (0)->SetAttribute ("ReceiveErrorModel", PointerValue (em1));
    // Assign ip addresses
    if2 = address2.Assign (devices2);
    address2.NewNetwork ();
    // setup ip routes
    cmd_oss.str ("");
    cmd_oss << "rule add from " << if2.GetAddress (0, 0) << " table " << (2);
    LinuxStackHelper::RunIp (nodes.Get (1), Seconds (0.1), cmd_oss.str ().c_str ());
    cmd_oss.str ("");
    cmd_oss << "route add 10.2." << 1 << ".0/24 dev sim" << 1 << " scope link table " << (2);
    LinuxStackHelper::RunIp (nodes.Get (1), Seconds (0.1), cmd_oss.str ().c_str ());
    cmd_oss.str ("");
    cmd_oss << "route add 10.1.0.0/16 via " << if2.GetAddress (1, 0) << " dev sim" << 1 << " table " << (2);
    LinuxStackHelper::RunIp (nodes.Get (1), Seconds (0.1), cmd_oss.str ().c_str ());
    cmd_oss.str ("");
    cmd_oss << "route add 10.2.0.0/16 via " << if2.GetAddress (1, 0) << " dev sim1";
    LinuxStackHelper::RunIp (routers.Get (0), Seconds (0.2), cmd_oss.str ().c_str ());
//      setPos (routers.Get (0), 70, 30, 0);
    setDirection(routers.Get(0), 5, 0, 0, 40, 30, 0);

    // H2 <-> H3
    devices3 = pointToPoint.Install (nodes.Get(1), nodes.Get(2));
    if3 = address3.Assign (devices3);
    address3.NewNetwork ();

    cmd_oss.str ("");
    cmd_oss << "rule add from " << if3.GetAddress (1, 0) << " table " << (2);
    LinuxStackHelper::RunIp (nodes.Get (1), Seconds (0.1), cmd_oss.str ().c_str ());

    // Tell H1 where H3 is
    cmd_oss.str ("");
    cmd_oss << "route add " << if3.GetAddress (1, 0) << " dev sim" << 0;
    LinuxStackHelper::RunIp (nodes.Get (0), Seconds (0.1), cmd_oss.str ().c_str ());

    // Tell router where H3 is
    cmd_oss.str ("");
    cmd_oss << "route add " << if3.GetAddress (1, 0) << " dev sim" << 1;
    LinuxStackHelper::RunIp (routers.Get (0), Seconds (0.1), cmd_oss.str ().c_str ());

    // Tell lte-router where H3 is
    Ipv4StaticRoutingHelper ipv4RoutingHelper;
    Ptr<Ipv4StaticRouting> remoteHostStaticRouting = ipv4RoutingHelper.GetStaticRouting (pgw->GetObject<Ipv4> ());
    remoteHostStaticRouting->AddNetworkRouteTo (Ipv4Address ("10.3.0.0"), Ipv4Mask ("255.255.255.0"),
                                                Ipv4Address ("10.2.0.1"), 3);



    devices4 = pointToPoint.Install (nodes.Get(1), tapNodes.Get(0));
    if4 = address4.Assign (devices4);
    address4.NewNetwork ();

    CsmaHelper csma;
    NetDeviceContainer csmaDevices = csma.Install (tapNodes);
//    if5 = address5.Assign (csmaDevices);
//    address5.NewNetwork();

    // H2 <-> H4
    // H2 <-> H5

    cmd_oss.str ("");
    cmd_oss << "route add 10.5.0.0/24 dev sim3";
    LinuxStackHelper::RunIp (nodes.Get (1), Seconds (0.1), cmd_oss.str ().c_str ());

    cmd_oss.str ("");
    cmd_oss << "addr add 10.5.0.1/24 dev sim1";
    LinuxStackHelper::RunIp (tapNodes.Get (0), Seconds (0.1), cmd_oss.str ().c_str ());
    cmd_oss.str ("");
    cmd_oss << "link set sim1 up";
    LinuxStackHelper::RunIp (tapNodes.Get (0), Seconds (0.1), cmd_oss.str ().c_str ());
    cmd_oss.str ("");
    cmd_oss << "route add 10.5.0.0/24 dev sim1";
    LinuxStackHelper::RunIp (tapNodes.Get (0), Seconds (0.1), cmd_oss.str ().c_str ());
    cmd_oss.str ("");
    cmd_oss << "route add 10.0.0.0/8 dev sim0";
    LinuxStackHelper::RunIp (tapNodes.Get (0), Seconds (0.1), cmd_oss.str ().c_str ());
//    cmd_oss.str ("");
//    cmd_oss << "route add 10.4.0.0/24 dev sim0";
//    LinuxStackHelper::RunIp (tapNodes.Get (0), Seconds (0.1), cmd_oss.str ().c_str ());

    LinuxStackHelper::RunIp (pgw, Seconds (5.1), "route");
    LinuxStackHelper::RunIp (pgw, Seconds (5.1), "a");

//    TapBridgeHelper tapBridge;
//    tapBridge.SetAttribute ("Mode", StringValue ("UseBridge"));
//    tapBridge.SetAttribute ("DeviceName", StringValue ("tap0"));
//    tapBridge.Install (tapNodes.Get (1), csmaDevices.Get (1));
//    tapBridge.SetAttribute ("DeviceName", StringValue ("tap1"));
//    tapBridge.Install (tapNodes.Get (2), csmaDevices.Get (2));


//    lteHelper->EnableRlcTraces ();

    // default route
    LinuxStackHelper::RunIp (nodes.Get (0), Seconds (0.1), "route add default via 7.0.0.1 dev sim0");
    LinuxStackHelper::RunIp (nodes.Get (1), Seconds (0.1), "route add default via 10.2.0.2 dev sim0");
    LinuxStackHelper::RunIp (nodes.Get (2), Seconds (0.1), "route add default via 10.3.0.1 dev sim0");
    LinuxStackHelper::RunIp (tapNodes.Get (0), Seconds (0.1), "route add default dev sim0");
    LinuxStackHelper::RunIp (nodes.Get (0), Seconds (0.1), "rule show");
    LinuxStackHelper::RunIp (nodes.Get (0), Seconds (5.1), "route show table all");
    LinuxStackHelper::RunIp (nodes.Get (1), Seconds (0.1), "rule show");
    LinuxStackHelper::RunIp (nodes.Get (1), Seconds (5.1), "route show table all");

    // debug
    stack.SysctlSet (nodes, ".net.mptcp.mptcp_debug", "1");

#if 1
    LinuxStackHelper::SysctlGet (nodes.Get (0), NanoSeconds (0),
                                 ".net.ipv4.tcp_available_congestion_control", &PrintTcpFlags);
    LinuxStackHelper::SysctlGet (nodes.Get (0), NanoSeconds (0),
                                 ".net.ipv4.tcp_rmem", &PrintTcpFlags);
    LinuxStackHelper::SysctlGet (nodes.Get (0), NanoSeconds (0),
                                 ".net.ipv4.tcp_wmem", &PrintTcpFlags);
    LinuxStackHelper::SysctlGet (nodes.Get (0), NanoSeconds (0),
                                 ".net.core.rmem_max", &PrintTcpFlags);
    LinuxStackHelper::SysctlGet (nodes.Get (0), NanoSeconds (0),
                                 ".net.core.wmem_max", &PrintTcpFlags);
    LinuxStackHelper::SysctlGet (nodes.Get (0), Seconds (1),
                                 ".net.ipv4.tcp_available_congestion_control", &PrintTcpFlags);
    LinuxStackHelper::SysctlGet (nodes.Get (0), Seconds (1),
                                 ".net.ipv4.tcp_rmem", &PrintTcpFlags);
    LinuxStackHelper::SysctlGet (nodes.Get (0), Seconds (1),
                                 ".net.ipv4.tcp_wmem", &PrintTcpFlags);
    LinuxStackHelper::SysctlGet (nodes.Get (0), Seconds (1),
                                 ".net.core.rmem_max", &PrintTcpFlags);
    LinuxStackHelper::SysctlGet (nodes.Get (0), Seconds (1),
                                 ".net.core.wmem_max", &PrintTcpFlags);
#endif
#if 1
    if (bufSize.length () != 0)
    {
        stack.SysctlSet (nodes, ".net.ipv4.tcp_rmem",
                         bufSize + " " + bufSize + " " + bufSize);
        //                       "4096 87380 " +bufSize);
        stack.SysctlSet (nodes, ".net.ipv4.tcp_wmem",
                         bufSize + " " + bufSize + " " + bufSize);
        stack.SysctlSet (nodes, ".net.core.rmem_max",
                         bufSize);
        stack.SysctlSet (nodes, ".net.core.wmem_max",
                         bufSize);
    }
#endif

    DceApplicationHelper dce;
    ApplicationContainer apps;

    dce.SetStackSize (1 << 20);

    // Launch iperf client on node 0
    dce.SetBinary ("iperf");
    dce.ResetArguments ();
    dce.ResetEnvironment ();
    dce.AddArgument ("-c");
    dce.AddArgument ("10.2.0.1");
    dce.ParseArguments ("-y C");
    dce.AddArgument ("-i");
    dce.AddArgument ("1");
    dce.AddArgument ("--time");
    dce.AddArgument ("40");
#if 0
    if (bufSize.length () != 0)
    {
      dce.AddArgument ("-w");
      dce.AddArgument (bufSize);
    }
#endif

    //apps = dce.Install (nodes.Get (0));
    //apps.Start (Seconds (5.0));
    //  apps.Stop (Seconds (15));

    // Launch iperf server on node 1
    dce.SetBinary ("iperf");
    dce.ResetArguments ();
    dce.ResetEnvironment ();
    dce.AddArgument ("-s");
//    dce.AddArgument ("-P");
//    dce.AddArgument ("1");
#if 0
    if (bufSize.length () != 0)
    {
      dce.AddArgument ("-w");
      dce.AddArgument (bufSize);
    }
#endif
    //apps = dce.Install (nodes.Get (1));
    //apps.Start (Seconds (4));
    apps = dce.Install (tapNodes.Get (0));
    apps.Start (Seconds (7));


    dce.SetBinary ("receiver");
    dce.ResetArguments ();
    dce.SetStackSize (1<<16);
    apps = dce.Install (nodes.Get (2));
    apps.Start (Seconds (1));

    dce.SetBinary ("spammer");
    dce.ResetArguments ();
    dce.AddArgument ("10.3.0.2");
    apps = dce.Install (nodes.Get (0));
    apps.Start (Seconds (4));

    dce.SetBinary ("ip");
    dce.ResetArguments ();
    dce.AddArgument ("route");
//    apps = dce.Install (nodes.Get (0));
//    apps.Start (Seconds (5));
//    apps = dce.Install (nodes.Get (1));
//    apps.Start (Seconds (5));
//    apps = dce.Install (nodes.Get (2));
//    apps.Start (Seconds (5));
//    apps = dce.Install (enbNodes.Get (0));
//    apps = dce.Install (pgw);
//    apps.Start (Seconds (5));
//    apps = dce.Install (tapNodes.Get (0));
//    apps.Start (Seconds (7));

    dce.SetBinary ("ip");
    dce.ResetArguments ();
    dce.AddArgument ("a");
//    dce.AddArgument ("rule");
//    dce.AddArgument ("list");
//    apps = dce.Install (tapNodes.Get (0));
//    apps.Start (Seconds (7));

    dce.SetBinary ("dropbear");
    dce.ResetArguments ();
    dce.ResetEnvironment ();
    dce.AddArgument ("-R");
//    dce.AddArgument ("-r");
//    dce.AddArgument ("/home/julek/dropbear-2016.74/ns3_rsa_db");
//    dce.AddArgument ("-E");
//    dce.AddArgument ("-s");
//    apps = dce.Install (nodes.Get (2));
//    apps.Start (Seconds (6));

    dce.SetBinary ("dbclient");
    dce.ResetArguments ();
    dce.ResetEnvironment ();
    dce.AddArgument ("-y");
    dce.AddArgument ("-l");
    dce.AddArgument ("root");
    dce.AddArgument ("10.3.0.2");
//    dce.AddArgument ("-i");
//    dce.AddArgument ("ns3_rsa_db");
//    apps = dce.Install (nodes.Get (1));
//    apps.Start (Seconds (8));

    dce.SetBinary ("ssh");
    dce.ResetArguments ();
    dce.ResetEnvironment ();
    dce.AddArgument ("root@10.3.0.2");
//    dce.AddArgument ("-i");
//    dce.AddArgument ("ns3_rsa_db");
//    apps = dce.Install (nodes.Get (1));
//    apps.Start (Seconds (8));


//    pointToPoint.EnablePcap ("mptcp-lte-wifi-server", nodes.Get(1)->GetDevice(0), false);
//    pointToPoint.EnablePcap ("mptcp-lte-wifi-server", nodes.Get(1)->GetDevice(1), false);
//    pointToPoint.EnablePcap ("mptcp-lte-wifi-server", nodes.Get(1)->GetDevice(2), false);
    pointToPoint.EnablePcap ("mptcp-lte-wifi-server", tapNodes.Get(0)->GetDevice(0), false);
    csma.EnablePcap ("mptcp-lte-wifi-server", tapNodes.Get(0)->GetDevice(1), false);
    phy.EnablePcap ("mptcp-lte-wifi", nodes.Get(0)->GetDevice(1), false);
    // pointToPoint.EnablePcapAll ("mptcp-lte-wifi", false);
    // phy.EnablePcapAll ("mptcp-lte-wifi", false);
    //lteHelper->EnableTraces ();

    // Output config store to txt format
    Config::SetDefault ("ns3::ConfigStore::Filename", StringValue ("output-attributes.txt"));
    Config::SetDefault ("ns3::ConfigStore::FileFormat", StringValue ("RawText"));
    Config::SetDefault ("ns3::ConfigStore::Mode", StringValue ("Save"));
    ConfigStore outputConfig2;
    outputConfig2.ConfigureDefaults ();
    outputConfig2.ConfigureAttributes ();

//  Simulator::Stop (Seconds (stopTime));
    Simulator::Stop (Seconds (10));

    NS_LOG_INFO ("Running simulation");
    Simulator::Run ();

    std::vector <ProcStatus> v = dceManager.GetProcStatus ();
    std::string ip_cmd("ip ");

    for (std::vector <ProcStatus>::iterator i = v.begin (); i != v.end () ; ++i) {
        ProcStatus st = *i;

        if (st.GetCmdLine().compare(0, ip_cmd.length(), ip_cmd) != 0) {
//        if (st.GetNode() == 0) {
            std::printf("Node: %d ; PID:%d ; CMD: %s\n", st.GetNode(), st.GetPid(), st.GetCmdLine().c_str());
        }
//        std::sprintf (std::stdoutname, "files-%d/var/log/%d/stdout", st.GetNode (), st.GetPid ());
    }

    Simulator::Destroy ();

    return 0;
}
