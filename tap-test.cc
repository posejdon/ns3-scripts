#include <iostream>
#include <fstream>

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/csma-module.h"
#include "ns3/tap-bridge-module.h"
#include "ns3/internet-module.h"
#include "ns3/dce-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/wifi-module.h"
#include "ns3/lte-module.h"
#include "ns3/mobility-module.h"
#include "ns3/applications-module.h"
#include "ns3/netanim-module.h"
#include "ns3/constant-position-mobility-model.h"
#include "ns3/config-store-module.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/ipv4-static-routing-helper.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("TapCsmaVirtualMachineExample");


void setDirection (Ptr<Node> n, int velx, int vely, int velz, int posx, int posy, int posz)
{
    Ptr<ConstantVelocityMobilityModel> dir = CreateObject<ConstantVelocityMobilityModel> ();
    n->AggregateObject (dir);
    Vector dirVec2 (velx, vely, velz);
    Vector locVec2 (posx, posy, posz);
    dir->SetPosition (locVec2);
    dir->SetVelocity (dirVec2);
}

void setPos (Ptr<Node> n, int x, int y, int z)
{
    Ptr<ConstantPositionMobilityModel> loc = CreateObject<ConstantPositionMobilityModel> ();
    n->AggregateObject (loc);
    Vector locVec2 (x, y, z);
    loc->SetPosition (locVec2);
}

void PrintTcpFlags (std::string key, std::string value)
{
    NS_LOG_INFO (key << "=" << value);
}

int main (int argc, char *argv[])
{
    CommandLine cmd;
    cmd.Parse (argc, argv);

    //
    // We are interacting with the outside, real, world.  This means we have to
    // interact in real-time and therefore means we have to use the real-time
    // simulator and take the time to calculate checksums.
    //
    GlobalValue::Bind ("SimulatorImplementationType", StringValue ("ns3::RealtimeSimulatorImpl"));
    GlobalValue::Bind ("ChecksumEnabled", BooleanValue (true));

    NodeContainer nodes;
    nodes.Create (4);
    NodeContainer tapNodes;
    tapNodes.Create(3);
    NodeContainer routers;
    routers.Create(2);

    std::ostringstream cmd_oss;

    DceApplicationHelper dce;
    dce.ResetArguments ();
    dce.ResetEnvironment ();

    ApplicationContainer apps;
    dce.SetStackSize (1 << 20);
    DceManagerHelper dceManager;
    dceManager.SetNetworkStack ("ns3::LinuxSocketFdFactory",
                                "Library", StringValue ("liblinux.so"));

    Ptr<LteHelper> lteHelper = CreateObject<LteHelper> ();
    PointToPointHelper pointToPoint;
    pointToPoint.SetDeviceAttribute ("DataRate", StringValue ("2Mbps"));
    pointToPoint.SetChannelAttribute ("Delay", StringValue ("10ms"));
    Ptr<RateErrorModel> em1 =
            CreateObjectWithAttributes<RateErrorModel> ("RanVar", StringValue ("ns3::UniformRandomVariable[Min=0.0|Max=1.0]"),
                                                        "ErrorRate", DoubleValue (0.01),
                                                        "ErrorUnit", EnumValue (RateErrorModel::ERROR_UNIT_PACKET)
            );
    Ipv4InterfaceContainer if1, if2, if3, if4, if5;
    Ptr<PointToPointEpcHelper> epcHelper = CreateObject<PointToPointEpcHelper> ();
    NetDeviceContainer devices1, devices2, devices3, devices4, devices5;

    dceManager.Install (nodes);
    dceManager.Install (routers);

    LinuxStackHelper stack;
    stack.Install (nodes);
    stack.Install (routers);

    Ipv4AddressHelper wifi0, wifi1, r0n2, r1n2, n3n2;
    wifi0.SetBase("10.4.0.0", "255.255.255.0");
    wifi1.SetBase("10.3.0.0", "255.255.255.0");
    r0n2.SetBase("10.6.0.0", "255.255.255.0");
    r1n2.SetBase("10.5.0.0", "255.255.255.0");
    n3n2.SetBase("10.8.0.0", "255.255.255.0");

//LogComponentEnable("SingleModelSpectrumChannel", LOG_LEVEL_ALL);
//LogComponentEnable("LteSpectrumPhy", LOG_LEVEL_DEBUG);
//LogComponentEnable("LteInterference", LOG_LEVEL_ALL);
//LogComponentEnable("PointToPointRemoteChannel", LOG_LEVEL_ALL);
//LogComponentEnable("Ipv4L3Protocol", LOG_LEVEL_ALL);
//LogComponentEnable("Ipv4Interface", LOG_LEVEL_ALL);
//LogComponentEnable("PointToPointNetDevice", LOG_LEVEL_ALL);
//LogComponentEnable("TapBridge", LOG_LEVEL_ALL);
//LogComponentEnable("LteUeNetDevice", LOG_LEVEL_ALL);
//LogComponentEnable("LteEnbNetDevice", LOG_LEVEL_ALL);
//LogComponentEnable("Ipv4ListRouting", LOG_LEVEL_ALL);
//LogComponentEnable("Ipv4StaticRouting", LOG_LEVEL_ALL);
//LogComponentEnable("Ipv4GlobalRouting", LOG_LEVEL_ALL);
//LogComponentEnable("LteRlc", LOG_LEVEL_DEBUG);
//LogComponentEnable("LteSpectrumPhy", LOG_LEVEL_ALL);



    // ************************************************************************
    //                                  WIFI
    // ************************************************************************

    setDirection (nodes.Get (0), -3, 0, 0, 0, 0, 0);
    setDirection (nodes.Get (1), -3, 0, 0, 0, 0, 0);
//    setPos (nodes.Get (0), 0, 0, 0);
//    setPos (nodes.Get (1), 0, 0, 0);
    setPos (routers.Get (0), 10, 0, 0);
    setPos (routers.Get (1), 10, 0, 0);

    WifiHelper wifi = WifiHelper::Default ();
    YansWifiPhyHelper phy = YansWifiPhyHelper::Default ();
    YansWifiChannelHelper phyChannel = YansWifiChannelHelper::Default ();
    NqosWifiMacHelper mac;
    phy.SetChannel (phyChannel.Create ());
    mac.SetType ("ns3::AdhocWifiMac");
    wifi.SetStandard (WIFI_PHY_STANDARD_80211a);


    // left connection wifi0
    devices1 = wifi.Install (phy, mac, NodeContainer (routers.Get (0), nodes.Get (0)));
    if1 = wifi0.Assign (devices1);
    wifi0.NewNetwork ();
    LinuxStackHelper::RunIp (routers.Get (0), Seconds (0.1), "route add 10.1.0.0/24 via 10.4.0.2 dev sim0");

    // right connection wifi0
    devices2 = pointToPoint.Install(routers.Get (0), nodes.Get (2));
    if2 = r0n2.Assign(devices2);
    r0n2.NewNetwork();
    LinuxStackHelper::RunIp (routers.Get (0), Seconds (0.1), "route add default via 10.6.0.2 dev sim1");
    LinuxStackHelper::RunIp (nodes.Get (2), Seconds (0.1), "route add 10.1.0.0/24 via 10.6.0.1 dev sim0");



    YansWifiPhyHelper phy2 = YansWifiPhyHelper::Default ();
    phy2.Set("TxGain", DoubleValue(15));
    phy2.Set("RxGain", DoubleValue(15));

    YansWifiChannelHelper phyChannel2 = YansWifiChannelHelper::Default ();
    NqosWifiMacHelper mac2;
    phy2.SetChannel (phyChannel2.Create ());
    mac2.SetType ("ns3::AdhocWifiMac");


    // left connection wifi1
    devices3 = wifi.Install (phy2, mac2, NodeContainer (routers.Get (1), nodes.Get (1)));
    if3 = wifi1.Assign (devices3);
    wifi1.NewNetwork ();
    LinuxStackHelper::RunIp (routers.Get (1), Seconds (0.1), "route add 10.2.0.0/24 via 10.3.0.2 dev sim0");

    // right connection wifi1
    devices4 = pointToPoint.Install(routers.Get (1), nodes.Get (2));
    if4 = r1n2.Assign(devices4);
    r1n2.NewNetwork();
    LinuxStackHelper::RunIp (routers.Get (1), Seconds (0.1), "route add default via 10.5.0.2 dev sim1");
    LinuxStackHelper::RunIp (nodes.Get (2), Seconds (0.1), "route add 10.2.0.0/24 via 10.5.0.1 dev sim1");


    // ************************************************************************
    //                                END WIFI
    // ************************************************************************


    // ************************************************************************
    //                                  CSMA
    // ************************************************************************

    CsmaHelper csma;
    NetDeviceContainer csma0 = csma.Install (NodeContainer(nodes.Get(0), tapNodes.Get(0)));

    LinuxStackHelper::RunIp (nodes.Get (0), Seconds (0.1), "addr add 10.1.0.2/24 dev sim1");
    LinuxStackHelper::RunIp (nodes.Get (0), Seconds (0.1), "link set sim1 up");
    LinuxStackHelper::RunIp (nodes.Get (0), Seconds (0.1), "route add default via 10.4.0.1 dev sim0");

    NetDeviceContainer csma1 = csma.Install (NodeContainer(nodes.Get(1), tapNodes.Get(1)));

    LinuxStackHelper::RunIp (nodes.Get (1), Seconds (0.1), "addr add 10.2.0.2/24 dev sim1");
    LinuxStackHelper::RunIp (nodes.Get (1), Seconds (0.1), "link set sim1 up");
    LinuxStackHelper::RunIp (nodes.Get (1), Seconds (0.1), "route add default via 10.3.0.1 dev sim0");


    NetDeviceContainer csma2 = csma.Install (NodeContainer(nodes.Get(2), tapNodes.Get(2)));

    LinuxStackHelper::RunIp (nodes.Get (2), Seconds (0.1), "addr add 10.7.0.2/24 dev sim2");
    LinuxStackHelper::RunIp (nodes.Get (2), Seconds (0.1), "link set sim2 up");


    LinuxStackHelper::RunIp (nodes.Get (2), Seconds (5), "a");
    LinuxStackHelper::RunIp (nodes.Get (2), Seconds (5), "route");

    // ************************************************************************
    //                                END CSMA
    // ************************************************************************


    // ************************************************************************
    //                                 SERVER
    // ************************************************************************

    devices5 = pointToPoint.Install(nodes.Get (3), nodes.Get (2));
    LinuxStackHelper::RunIp (nodes.Get (2), Seconds (0.1), "addr add 10.8.0.2/24 dev sim3");
    LinuxStackHelper::RunIp (nodes.Get (2), Seconds (0.1), "link set sim3 up");
    LinuxStackHelper::RunIp (nodes.Get (3), Seconds (0.1), "addr add 10.8.0.1/24 dev sim0");
    LinuxStackHelper::RunIp (nodes.Get (3), Seconds (0.1), "link set sim0 up");
//    if5 = n3n2.Assign(devices5);
//    n3n2.NewNetwork();
    LinuxStackHelper::RunIp (nodes.Get (3), Seconds (0.1), "route add default via 10.8.0.2 dev sim0");
    LinuxStackHelper::RunIp (nodes.Get (2), Seconds (0.1), "route add 10.8.0.0/24 dev sim3");


    // ************************************************************************
    //                                END SERVER
    // ************************************************************************


    // ************************************************************************
    //                               TAP Bridge
    // ************************************************************************

    TapBridgeHelper tapBridge;
    tapBridge.SetAttribute ("Mode", StringValue ("UseBridge"));
    tapBridge.SetAttribute ("DeviceName", StringValue ("tap0"));
    tapBridge.Install (tapNodes.Get (0), csma0.Get (1));
    tapBridge.SetAttribute ("DeviceName", StringValue ("tap1"));
    tapBridge.Install (tapNodes.Get (1), csma1.Get (1));

    tapBridge.SetAttribute ("DeviceName", StringValue ("tap2"));
    tapBridge.Install (tapNodes.Get (2), csma2.Get (1));

    // ************************************************************************
    //                             End TAP Bridge
    // ************************************************************************

    stack.SysctlSet (nodes, ".net.mptcp.mptcp_enabled", "0");

//    csma.EnablePcap ("pcap-output/csmaNodes", nodes.Get(0)->GetDevice(1), false);
//    csma.EnablePcap ("pcap-output/csmaNodes", nodes.Get(1)->GetDevice(1), false);
    pointToPoint.EnablePcap ("pcap-output/internet-node", nodes.Get(2)->GetDevice(0), false);
    pointToPoint.EnablePcap ("pcap-output/internet-node", nodes.Get(2)->GetDevice(1), false);
    csma.EnablePcap ("pcap-output/internet-node", nodes.Get(2)->GetDevice(2), false);
    pointToPoint.EnablePcap ("pcap-output/internet-node", nodes.Get(2)->GetDevice(3), false);
//    phy.EnablePcap ("pcap-output/router", routers.Get(0)->GetDevice(0), false);
//    pointToPoint.EnablePcap ("pcap-output/router", routers.Get(0)->GetDevice(1), false);
//    phy.EnablePcap ("pcap-output/router", routers.Get(1)->GetDevice(0), false);
//    pointToPoint.EnablePcap ("pcap-output/router", routers.Get(1)->GetDevice(1), false);



    dce.SetBinary ("receiver");
    dce.ResetArguments ();
    dce.SetStackSize (1<<16);
    apps = dce.Install (nodes.Get (3));
    apps.Start (Seconds (1));

    dce.SetBinary ("spammer");
    dce.ResetArguments ();
    dce.AddArgument ("10.3.0.1");
//    apps = dce.Install (nodes.Get (1));
//    apps.Start (Seconds (4));


    dce.SetBinary ("iperf");
    dce.ResetArguments ();
    dce.AddArgument ("-s");
//    apps = dce.Install (nodes.Get (0));
//    apps.Start (Seconds (2));

    dce.SetBinary ("iperf");
    dce.ResetArguments ();
    dce.AddArgument ("-c");
    dce.AddArgument ("10.4.0.2");
//    apps = dce.Install (nodes.Get (0));
//    apps.Start (Seconds (6));


    Simulator::Stop (Seconds (10000));
    NS_LOG_UNCOND ("Running tap-test");
    Simulator::Run ();

    std::vector <ProcStatus> v = dceManager.GetProcStatus ();
    std::string ip_cmd("ip ");

    for (std::vector <ProcStatus>::iterator i = v.begin (); i != v.end () ; ++i) {
        ProcStatus st = *i;

//        if (st.GetCmdLine().compare(0, ip_cmd.length(), ip_cmd) != 0) {
//        if (st.GetNode() == 0) {
            std::printf("Node: %d ; PID:%d ; CMD: %s\n", st.GetNode(), st.GetPid(), st.GetCmdLine().c_str());
//        }
//        std::sprintf (std::stdoutname, "files-%d/var/log/%d/stdout", st.GetNode (), st.GetPid ());
    }

    Simulator::Destroy ();
}
