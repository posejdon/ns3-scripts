/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

//
// This is an illustration of how one could use virtualization techniques to
// allow running applications on virtual machines talking over simulated
// networks.
//
// The actual steps required to configure the virtual machines can be rather
// involved, so we don't go into that here.  Please have a look at one of
// our HOWTOs on the nsnam wiki for more details about how to get the
// system confgured.  For an example, have a look at "HOWTO Use Linux
// Containers to set up virtual networks" which uses this code as an
// example.
//
//
#include <iostream>
#include <fstream>

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/csma-module.h"
#include "ns3/tap-bridge-module.h"
#include "ns3/internet-module.h"
#include "ns3/dce-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/wifi-module.h"
#include "ns3/lte-module.h"
#include "ns3/mobility-module.h"
#include "ns3/applications-module.h"
#include "ns3/netanim-module.h"
#include "ns3/constant-position-mobility-model.h"
#include "ns3/config-store-module.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("TapCsmaVirtualMachineExample");


void setPos (Ptr<Node> n, int x, int y, int z)
{
    Ptr<ConstantPositionMobilityModel> loc = CreateObject<ConstantPositionMobilityModel> ();
    n->AggregateObject (loc);
    Vector locVec2 (x, y, z);
    loc->SetPosition (locVec2);
}

int
main (int argc, char *argv[])
{
    CommandLine cmd;
    cmd.Parse (argc, argv);

    //
    // We are interacting with the outside, real, world.  This means we have to
    // interact in real-time and therefore means we have to use the real-time
    // simulator and take the time to calculate checksums.
    //
    GlobalValue::Bind ("SimulatorImplementationType", StringValue ("ns3::RealtimeSimulatorImpl"));
    GlobalValue::Bind ("ChecksumEnabled", BooleanValue (true));

    NodeContainer nodes;
    nodes.Create (3);
    NodeContainer tapNodes;
    tapNodes.Create(2);

    std::ostringstream cmd_oss;

    DceManagerHelper dceManager;
    dceManager.SetNetworkStack ("ns3::LinuxSocketFdFactory",
                                "Library", StringValue ("liblinux.so"));

    Ptr<LteHelper> lteHelper = CreateObject<LteHelper> ();
    PointToPointHelper pointToPoint;
    Ptr<RateErrorModel> em1 =
            CreateObjectWithAttributes<RateErrorModel> ("RanVar", StringValue ("ns3::UniformRandomVariable[Min=0.0|Max=1.0]"),
                                                        "ErrorRate", DoubleValue (0.01),
                                                        "ErrorUnit", EnumValue (RateErrorModel::ERROR_UNIT_PACKET)
            );
    Ipv4InterfaceContainer if1, if2;
    Ptr<PointToPointEpcHelper> epcHelper = CreateObject<PointToPointEpcHelper> ();
    NetDeviceContainer devices1, devices2;

    NodeContainer enbNodes;
    enbNodes.Create(1);

    dceManager.Install (enbNodes);
    dceManager.Install (nodes);

    LinuxStackHelper stack;
    stack.Install (nodes);


    // H0 <-> LTE-R
    // Left link
    lteHelper->SetEpcHelper (epcHelper);
    Ptr<Node> pgw = epcHelper->GetPgwNode ();
    dceManager.Install(pgw);
    setPos (enbNodes.Get(0), 60, -4000, 0);
    setPos (nodes.Get(0), -20, 30 / 2, 0);

    NetDeviceContainer enbLteDevs = lteHelper->InstallEnbDevice (enbNodes);
    NetDeviceContainer ueLteDevs = lteHelper->InstallUeDevice (nodes.Get(0));

    // Assign ip addresses
    if1 = epcHelper->AssignUeIpv4Address (NetDeviceContainer (ueLteDevs));
    lteHelper->Attach (ueLteDevs.Get(0), enbLteDevs.Get(0));

    // setup ip routes
//    LinuxStackHelper::RunIp (nodes.Get(0), Seconds (0.1), "route add default via 7.0.0.1 dev sim0");


    TapBridgeHelper tapBridge;
    tapBridge.SetAttribute ("Mode", StringValue ("UseBridge"));
    tapBridge.SetAttribute ("DeviceName", StringValue ("tap0"));
    tapBridge.Install (nodes.Get (0), ueLteDevs.Get (0));


    // LTE-R <-> H2
    // Right link
//    devices2 = pointToPoint.Install (pgw, nodes.Get (2));
//    setPos (pgw, 70, 0, 0);
//    devices2.Get (0)->SetAttribute ("ReceiveErrorModel", PointerValue (em1));
//    // Assign ip addresses
//    Ipv4AddressHelper lteNode2Addr;
//    lteNode2Addr.SetBase ("10.4.0.0", "255.255.255.0");
//    if2 = lteNode2Addr.Assign (devices2);
//    lteNode2Addr.NewNetwork ();
    // setup ip routes
//    LinuxStackHelper::RunIp (nodes.Get(2), Seconds (0.1), "route add 10.4.0.0/24 dev sim0 scope link");
//    LinuxStackHelper::RunIp (nodes.Get(2), Seconds (0.1), "route add default via 10.4.0.1 dev sim0");
//



    // Run the simulation for ten minutes to give the user time to play around
    //
    Simulator::Stop (Seconds (100));
    NS_LOG_UNCOND ("Running tap-test");
    Simulator::Run ();

    std::vector <ProcStatus> v = dceManager.GetProcStatus ();
    std::string ip_cmd("ip ");

    for (std::vector <ProcStatus>::iterator i = v.begin (); i != v.end () ; ++i) {
        ProcStatus st = *i;

//        if (st.GetCmdLine().compare(0, ip_cmd.length(), ip_cmd) != 0) {
//        if (st.GetNode() == 0) {
        std::printf("Node: %d ; PID:%d ; CMD: %s\n", st.GetNode(), st.GetPid(), st.GetCmdLine().c_str());
//        }
//        std::sprintf (std::stdoutname, "files-%d/var/log/%d/stdout", st.GetNode (), st.GetPid ());
    }

    Simulator::Destroy ();

}